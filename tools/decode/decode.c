#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
	unsigned int decoded;
	char buf[64], *p1;
	size_t i, len;
	FILE *fp;

	if (argv[1] == NULL) {
		return (1);
	}

	fp = fopen(argv[1], "r");
	if (fp == NULL) {
		return (1);
	}

	while (!feof(fp)) {
		memset(buf, 0, sizeof(buf));
		fgets(buf, sizeof(buf)-1, fp);
		len = strlen(buf);
		for (i = 0; i < len; i += 3) {
			sscanf(&buf[i], "%02x", &decoded);
			printf("%c", decoded);
		}
	}

	fclose(fp);

	return (0);
}
